require 'roo'

@prod_folder = 'producoes'
@ori_folder = 'orientacoes'
@completed_files = Array.new
@count = 0

def parse_ppgs
  @directory = '/home/henrique/Documentos/relatorios_sucupira'
  count = 0
  Dir.foreach(@directory) do |dir_name|
    puts 'Fazendo area ' + dir_name
    Dir.foreach(@directory + "/" + dir_name) do |file_name|
      if (file_name.include?(".xlsx") && !@completed_files.include?(file_name) && !file_name.include?("lock"))
        puts 'Arquivo ' + file_name
        producao_intelectual(dir_name, file_name) unless File.exists? ("#{@directory}/#{dir_name}/#{@prod_folder}/producao_intelectual_#{@count}.csv")
        trabalho_conclusoes(dir_name, file_name) unless File.exists? ("#{@directory}/#{dir_name}/#{@prod_folder}/trabalho_de_conclusao_#{@count}.csv")
        @count += 1
      end
    end
  end
end

def producao_intelectual(dir_name, file_name)
  Dir.mkdir(@directory + '/' + dir_name + '/' + @prod_folder) unless File.exists? (@directory + '/' + dir_name + '/' + @prod_folder)
  `xlsx2csv -c utf8 -s 12 '/home/henrique/Documentos/relatorios_sucupira/#{dir_name}/#{file_name}' > '#{@directory}/#{dir_name}/#{@prod_folder}/producao_intelectual_#{@count}.csv'`
end

def trabalho_conclusoes(dir_name, file_name)
  Dir.mkdir(@directory + '/' + dir_name + '/' + @ori_folder) unless File.exists? (@directory + '/' + dir_name + '/' + @ori_folder)
  `xlsx2csv -c utf8 -s 11 '/home/henrique/Documentos/relatorios_sucupira/#{dir_name}/#{file_name}' > '#{@directory}/#{dir_name}/#{@ori_folder}/trabalho_de_conclusao_#{@count}.csv'`
end

parse_ppgs